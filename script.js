import { config } from "./jeu-video-en-javascript/src/js/modules/config.js";
import { displayScore } from "./jeu-video-en-javascript/src/js/modules/ScoreBoard.js";

const requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;
const canvas = document.getElementById("canvas");
canvas.width = window.innerWidth;
canvas.height = 240;
const ctx = canvas.getContext("2d");
let raf;

const tete = new Image();
tete.src = `./jeu-video-en-javascript/${config.imagePath.characters}/char1.png`;

const ball = {
    x: 5, //position sur axe x
    y: 100, //position sur axe y
    vx: 4, //vitesse de deplacement sur axe x
    vy: 10, //vitesse de deplacement sur axe y

    draw: function () {
        ctx.drawImage(tete, this.x, this.y, 100, 85); // pour aujouter image sur mon objet
    },
};

function draw() {
    ctx.clearRect(0, 0, window.innerWidth, canvas.height);
    ball.draw();
    ball.x += ball.vx;
    ball.y += ball.vy;
    ball.vy *= 0.99; //pour ralentir vitesse verticale et donc le rebond
    ball.vy += 0.25; //pour ralentir vitesse verticale et donc le rebond
    raf = window.requestAnimationFrame(draw);

    if (ball.y + ball.vy > canvas.height - 80 || ball.y + ball.vy < 0) {
        ball.vy = -ball.vy;
    }
    if (ball.x + ball.vx > window.innerWidth - 90 || ball.x + ball.vx < 0) {
        ball.vx = -ball.vx;
    }
}

raf = window.requestAnimationFrame(draw);
ball.draw();

/* --> gestion du scoreboard + localstorage <-- */

const submitBtn = document.querySelector(".submitBtn");

const introMusic = document.getElementById("intro");
introMusic.loop = true;
introMusic.volume = 0.5;

//Fonction permettant de trier un tableau d'objet
//en fonction de la propriété entrée en paramètre
const dynamicSort = (property) => {
    let sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substring(1);
    }
    return (a, b) => {
        const result =
            a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
        return result * sortOrder;
    };
};

const tableBody = document.querySelector("tbody");
const getLocalStorageScoreboard = JSON.parse(
    localStorage.getItem("scoreboard")
);

if (localStorage.getItem("scoreboard") !== null) {
    getLocalStorageScoreboard.sort(dynamicSort("time"));
    const nbPlayerInScoreboard = 10; //10 car on veut récupérer les 10 meilleurs temps dans notre tableau

    getLocalStorageScoreboard.forEach((player, i) => {
        if (i < nbPlayerInScoreboard) {
            tableBody.innerHTML += `
            <tr>
                <td>${i + 1}</td>
                <td>${player.name}</td>
                <td>${player.time} sec</td>
            </tr>
        `;
        }
    });
}

//représente chaque objet auquel on a associé un chiffre.

// exécuter la fonction quand le bouton est cliqué
submitBtn.addEventListener("click", function () {
    localStorage.setItem("actualLevel", 1);
});
displayScore();

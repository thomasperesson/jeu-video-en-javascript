import { initWorld } from "./src/js/InitWorld.js";
if(localStorage.getItem('actualLevel') === null) {
    localStorage.setItem('actualLevel', 1);
}
initWorld(localStorage.getItem('actualLevel'));

const retryButton = document.querySelector('.retryButton');
const goHome = document.querySelector('.goHome');
retryButton.addEventListener('click', () => {
    document.location.href = document.location.href;
})

goHome.addEventListener('click', () => {
    document.location.href = "https://lesdieuxdudev.lecoledunumerique.fr/jeux/ez-game/";
})
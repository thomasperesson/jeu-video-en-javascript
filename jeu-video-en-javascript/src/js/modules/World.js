import { Render, engine, Runner } from "../MatterModule.js";

export function drawWorld() {
    const render = Render.create({
        element: document.body,
        engine: engine,
        options: {
            width: 1900,
            height: 900,
            pixelRatio: 1,
            enabled: true,
            wireframes: false,
            // showVelocity: true,
            // showAngleIndicator: true,
            // showAxes: true,
            // showCollisions: true,
            // showConvexHulls: true,
        },
    });
    // run the engine
    Runner.run(engine, Runner);
    // run the renderer
    Render.run(render);
    return render;
}
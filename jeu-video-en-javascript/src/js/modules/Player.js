import { Composite, Bodies, engine } from "../MatterModule.js";
import { addControl } from "./Control.js";
import { Collisions } from "./Collisions.js";
import { config } from "./config.js";

if (localStorage.getItem("actualLevel") === null) {
    localStorage.setItem("actualLevel", 1);
}

let playerCoordonate;
switch (localStorage.getItem("actualLevel")) {
    case "1":
        playerCoordonate = config.startPlayer.map1;
        break;
    case "2":
        playerCoordonate = config.startPlayer.map2;
        break;
    default:
        playerCoordonate = config.startPlayer.map1;
        break;
}

const playerImage = new Image();
playerImage.src = `${config.imagePath.characters}/char1.png`;

export const player = Bodies.circle(
    playerCoordonate.x,
    playerCoordonate.y,
    25, {
        label: "player",
        render: {
            sprite: {
                texture: playerImage.src,
                xScale: 0.52,
                yScale: 0.52,
            },
        },
        density: 0.02,
        friction: 0.1,
        frictionStatic: 0,
        frictionAir: 0.01,
        restitution: 0.5,
        ground: false,
    }

);

export function initPlayer() {
    if (localStorage.getItem("actualLevel") !== "1") {
        Composite.add(engine.world, player);
    } else {
        addControl(player);
        Composite.add(engine.world, player);
        Collisions.addCollisions(player);
    }
}
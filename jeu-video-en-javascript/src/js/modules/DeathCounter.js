export const showDeath = () => {
    const deathCounter = document.getElementById("deathCounter");
    deathCounter.innerText = localStorage.getItem("deathCounter");
}
let deathCount = 0;
export const incrementDeath = () => {
    
    if (localStorage.getItem('actualLevel') === "1") {
        deathCount++;
        localStorage.setItem("deathCounter", Math.round(deathCount / 4)); // matter compte 2 tick lors d'une collision donc on divise par 2 pour ne compter qu'une mort
    } else {
        console.log('Death');
        deathCount++;
        localStorage.setItem("deathCounter", deathCount);
    }
    
}
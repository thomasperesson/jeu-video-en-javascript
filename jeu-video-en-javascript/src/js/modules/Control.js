import { Events, Body, engine } from "../MatterModule.js";
import { player } from "./Player.js";

export function addControl() {
    let keys = [];
    window.addEventListener("keydown", function (e) {
        keys[e.key] = true;
    });
    window.addEventListener("keyup", function (e) {
        keys[e.key] = false;
    });
    //main engine update loop
    Events.on(engine, "beforeUpdate", function () {
        //jump
        if ((keys["z"] || keys[" "] || keys["ArrowUp"]) && player.ground) {
            player.force = { x: 0, y: -0.08 };
        }
        //spin left and right
        if ((keys["q"] || keys["ArrowLeft"]) && player.angularVelocity > -0.2) {
            Body.translate(player, {
                x: -10,
                y: 0,
            });
        } else {
            if (
                (keys["d"] || keys["ArrowRight"]) &&
                player.angularVelocity < 0.2
            ) {
                Body.translate(player, {
                    x: +10,
                    y: 0,
                });
            }
        }
    });
}

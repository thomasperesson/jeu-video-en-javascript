const start_at = new Date();

// Update the count down every 1 second
let timerTimestamp = setInterval(function() {

    // Get todays date and time
    let now = new Date();
    
    // Find the distance between now an the count down date
    let distance = now - start_at;
	
    
    // Time calculations for days, hours, minutes and seconds
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    let milliseconds = Math.floor(distance);
    if(milliseconds > seconds * 1000) {
        milliseconds = milliseconds - (seconds * 1000);
    }
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").value = hours + "h "
    + minutes + "m " + seconds + "s " + milliseconds + " ms";
}, 100);

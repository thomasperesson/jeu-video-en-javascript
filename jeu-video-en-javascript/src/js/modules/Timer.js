/*  ----> Timer <---- */
let seconds = 0;
let centiSeconds = 0;

function augmenterTemps() {
    centiSeconds++;
    if (centiSeconds === 100) {
        centiSeconds = 0;
        seconds++;
    }
}

let starter;
function startTimer() {
    starter = setInterval(augmenterTemps, 10); //setInterval = méthode JS pour mettre à jour le timer ttes les secondes
}

export function resetTimer() {
    centiSeconds = 0;
    seconds = 0;
    augmenterTemps();
}
function display() {
    const timerElement = document.getElementById("timer"); //on va chercher et on sélectionne élément dont ID=Affichage
    let secCentiSeparator = ":";
    let zeroBeforeSec = "";

    if (seconds < 10) {
        zeroBeforeSec = "00";
    } else if (seconds < 100) {
        zeroBeforeSec = "0";
    }
    if (centiSeconds < 10) {
        secCentiSeparator = ":0";
    }

    timerElement.innerText = `${zeroBeforeSec}${seconds}${secCentiSeparator}${centiSeconds}`;
}

setInterval(display, 10);

document.addEventListener("click", () => {
    clearInterval(augmenterTemps);
});

export function initTimer() {
    let timerStart = true;
    document.addEventListener("keydown", (e) => {
        if (
            e.key === "ArrowLeft" ||
            e.key === "ArrowRight" ||
            e.key === "ArrowUp" ||
            e.key === "q" ||
            e.key === "d" ||
            e.key === "z"
        ) {
            if (timerStart) {
                startTimer(); //démarrage fonction
                timerStart = false;
            }
        }
    });
    if (centiSeconds !== undefined) {
        display();
    }
}

export function stopTimer() {
    clearInterval(starter);
}



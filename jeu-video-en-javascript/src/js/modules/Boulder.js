import { Composite, Bodies, engine } from "../MatterModule.js";
import { Collisions } from "./Collisions.js";
import { config } from "./config.js";




let boulderCoordonate;
switch (localStorage.getItem("actualLevel")) {
    case "1":
        boulderCoordonate = config.startBoulder.map1;
        break;
    default:
        boulderCoordonate = config.startBoulder.map1;
        break;
}

const lavaboulderImage = new Image();
lavaboulderImage.src = "./src/img/ennemis/lavaboulder.png";

export const boulder = Bodies.circle(
    boulderCoordonate.x,
    boulderCoordonate.y,
    35, {
        label: "boulder",
        render: {
            sprite: {
                texture: lavaboulderImage.src,
                xScale: 0.1,
                yScale: 0.1,
            },
        },
        density: 1,
        friction: 0.5,
        frictionStatic: 0,
        frictionAir: 0.01,
        restitution: 0.5,
    }
);
export function initBoulder() {
    Composite.add(engine.world, boulder);
    Collisions.addCollisions(boulder);
}
export const config = {
    startPlayer: {
        map1: {
            x: 150,
            y: 850,
        },
        map2: {
            x: 150,
            y: 850,
        },
    },
    startBoulder: {
        map1: {
            x: 980,
            y: 10,
        },
    },
    startPlayerDev: {
        x: 1700,
        y: 850,
    },
    imagePath: {
        map1: "./src/img/maps/map1",
        map2: "./src/img/maps/map2",
        characters: "./src/img/characters",
    },
    soundPath: {
        general: "./src/sounds",
        map1: "./src/sounds/maps/map1",
        map2: "./src/sounds/maps/map2",
        characters: "./src/sounds/characters",
    },
    backgroundImagePath: {
        // map1: 'url("./src/img/maps/map1/backgrounds/background01.png")',
        // map2: 'url("./src/img/maps/map2/backgrounds/background02.png")',
    },
};

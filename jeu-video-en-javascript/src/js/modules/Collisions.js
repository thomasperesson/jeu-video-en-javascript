import { Events, engine, Body, Composite } from "../MatterModule.js";
import { stopTimer, resetTimer } from "../modules/Timer.js";
import { config } from "../modules/config.js";
import { initSecondLevel } from "../maps/map2/SecondLevel.js";
import { initPlayer } from "../modules/Player.js";
import { drawFinish } from "../elements/Finish.js";
import { boulder } from "../modules/Boulder.js";
import { $_GET } from "../modules/ScoreBoard.js";

let jump;
jump = new Audio(`${config.soundPath.general}/wallContact.wav`);
let death;
death = new Audio(`${config.soundPath.general}/death.wav`);
death.volume = 0.5;

let playerCoordonate;
switch (localStorage.getItem("actualLevel")) {
    case "1":
        playerCoordonate = config.startPlayer.map1;
        break;
    case "2":
        playerCoordonate = config.startPlayer.map2;

        break;
}

export class Collisions {
    static addCollisions(player) {
        //at the start of a collision for player
        Events.on(engine, "collisionStart", function (event) {
            let boulderCoordonate;
            let playerCoordonate;
            switch (localStorage.getItem("actualLevel")) {
                case "1":
                    playerCoordonate = config.startPlayer.map1;
                    boulderCoordonate = config.startBoulder.map1;
                    break;
                    
                case "2":
                    playerCoordonate = config.startPlayer.map2;
                    break;
            }
            let pairs = event.pairs;
            for (let i = 0, j = pairs.length; i != j; ++i) {
                let pair = pairs[i];
                if (pair.bodyA === player) {
                    player.ground = true;
                } else if (pair.bodyB === player) {
                    player.ground = true;
                }
                if (
                    (pair.bodyA.label === "player" &&
                        pair.bodyB.label === "trap") ||
                    (pair.bodyA.label === "player" &&
                        pair.bodyB.label === "boulder")
                ) {
                    death.play();
                    if (localStorage.getItem("actualLevel") === "1") {
                        resetTimer();
                    }

                    //Remettre le Player au point de départ
                    Body.setStatic(player, true);
                    Body.setPosition(player, {
                        x: playerCoordonate.x,
                        y: playerCoordonate.y,
                    });
                    Body.setStatic(player, false);
                }
                if (
                    (pair.bodyA.label === "boulder" &&
                        pair.bodyB.label === "trap") ||
                    (pair.bodyA.label === "player" &&
                        pair.bodyB.label === "boulder") ||
                    (pair.bodyA.label === "boulder" &&
                        pair.bodyB.label === "replaceboulder")
                ) {
                    //Remettre le Player au point de départ
                    Body.setStatic(boulder, true);
                    Body.setPosition(boulder, {
                        x: boulderCoordonate.x,
                        y: boulderCoordonate.y,
                    });
                    Body.setStatic(boulder, false);
                }
                if (
                    (pair.bodyA.label === "player" &&
                        pair.bodyB.label === "wall") ||
                    (pair.bodyA.label === "player" &&
                        pair.bodyB.label === "replaceboulder")
                ) {
                    jump.play();
                }
                if (pair.bodyA === player && pair.bodyB.label === "nextlevel") {
                    Composite.remove(engine.world, player);
                    localStorage.setItem("actualLevel", 2);
                    Composite.clear(engine.world, false);
                    initSecondLevel();
                    initPlayer();
                    Body.setPosition(player, {
                        x: config.startPlayer.map2.x,
                        y: config.startPlayer.map2.y,
                    });
                    drawFinish();
                }
                if (pair.bodyA === player && pair.bodyB.label === "finish") {
                    stopTimer();
                    Composite.remove(engine.world, player);
                    localStorage.setItem("actualLevel", 1);

                    /**
                     * Enregistrement du score dans le local storage
                     */
                    const playerName = $_GET("player")
                        .replace("+", " ")
                        .replace("%C3%A9", "é")
                        .replace("%C3%A8", "è");
                    const timerText =
                        document.getElementById("timer").innerText;
                    const dynamicSort = (property) => {
                        let sortOrder = 1;
                        if (property[0] === "-") {
                            sortOrder = -1;
                            property = property.substring(1);
                        }
                        return (a, b) => {
                            const result =
                                a[property] < b[property]
                                    ? -1
                                    : a[property] > b[property]
                                    ? 1
                                    : 0;
                            return result * sortOrder;
                        };
                    };
                    const displayScore = (scoreArray) => {
                        const tableBody = document.getElementById("tableBody");
                        scoreArray.forEach((onePlayer, index) => {
                            if (index < 10) {
                                tableBody.innerHTML += `
                                    <tr>
                                        <td>${index + 1}</td>
                                        <td>${onePlayer.name}</td>
                                        <td>${onePlayer.time}</td>
                                    </tr>
                                `;
                            }
                        });
                        const displayScoreElement =
                            document.querySelector(".displayScore");
                        displayScoreElement.style.display = "block";
                    };

                    if (localStorage.getItem("scoreboard") !== null) {
                        let scoreboard = JSON.parse(
                            localStorage.getItem("scoreboard")
                        );
                        const duplicate = scoreboard.some((item) => {
                            return (
                                JSON.stringify(item) ===
                                JSON.stringify({
                                    name: playerName,
                                    time: timerText,
                                })
                            );
                        });
                        if (!duplicate) {
                            scoreboard.push({
                                name: playerName,
                                time: timerText,
                            });
                            scoreboard.sort(dynamicSort("time"));
                            localStorage.setItem(
                                "scoreboard",
                                JSON.stringify(scoreboard)
                            );
                            displayScore(scoreboard);
                        }
                        const successElement =
                            document.querySelector(".success");
                        successElement.innerText = "Congratulations " + playerName;
                        const showPersonalScoreElement =
                            document.querySelector(".showPersonalScore");
                        showPersonalScoreElement.innerText =
                            "Your score is " + timerText + " secondes";
                    } else {
                        let scoreboard = [];
                        scoreboard.push({
                            name: playerName,
                            time: timerText,
                        });
                        scoreboard.sort(dynamicSort("time"));
                        localStorage.setItem(
                            "scoreboard",
                            JSON.stringify(scoreboard)
                        );
                        const successElement =
                            document.querySelector(".success");
                        successElement.innerText = "Congratulations " + playerName;
                        const showPersonalScoreElement =
                            document.querySelector(".showPersonalScore");
                        showPersonalScoreElement.innerText =
                            "Your score is " + timerText + " secondes";
                        displayScore(scoreboard);
                    }
                    /** Fin de l'enregistrement **/
                    console.log(document.location.href);
                }
            }
        });

        //ongoing checks for collisions for player
        Events.on(engine, "collisionActive", function (event) {
            let pairs = event.pairs;
            for (let i = 0, j = pairs.length; i != j; ++i) {
                let pair = pairs[i];
                if (pair.bodyA === player) {
                    player.ground = true;
                } else if (pair.bodyB === player) {
                    player.ground = true;
                }
            }
        });

        //at the end of a collision for player
        Events.on(engine, "collisionEnd", function (event) {
            let pairs = event.pairs;
            for (let i = 0, j = pairs.length; i != j; ++i) {
                let pair = pairs[i];
                if (pair.bodyA === player) {
                    player.ground = false;
                } else if (pair.bodyB === player) {
                    player.ground = false;
                }
            }
        });
        return player;
    }
}

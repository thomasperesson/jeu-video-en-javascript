import { config } from "./config.js";

let firstlevelmusic;
firstlevelmusic = new Audio(`${config.soundPath.general}/musique.mp3`);
firstlevelmusic.loop = true;
firstlevelmusic.volume = 0.5;

let secondlevelmusic;
secondlevelmusic = new Audio(`${config.soundPath.general}/level2zik.mp3`);
secondlevelmusic.loop = true;
secondlevelmusic.volume = 0.5;

export function initMusic(actualLevel) {
    switch (actualLevel) {
        case "1":
            secondlevelmusic.play();
            break;
        case "2":
            Object.keys(secondlevelmusic);
            secondlevelmusic.pause();
            Object.keys(secondlevelmusic);
            firstlevelmusic.play();
            break;
            
    }
};
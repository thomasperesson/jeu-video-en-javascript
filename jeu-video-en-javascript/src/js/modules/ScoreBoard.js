const scoreBoardArray = [];
const tableBody = document.querySelector("tbody");

export const $_GET = (param) => {
    let vars = {};
    window.location.href.replace(location.hash, "").replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function (m, key, value) {
            // callback
            vars[key] = value !== undefined ? value : "";
        }
    );
    if (param) {
        return vars[param] ? vars[param] : null;
    }
    return vars.replace("+", " ");
};

const dynamicSort = (property) => {
    let sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substring(1);
    }
    return (a, b) => {
        const result =
            a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
        return result * sortOrder;
    };
};

let playerName = $_GET("player");
// scoreBoardArray.push({
//     name: playerName,
//     time: Number(document.querySelector("#timer").innerText),
//     nbDeath: Number(document.querySelector("#deathCounter").innerText),
// });

// export const saveScore = (arr) => {
//     localStorage.setItem("scoreboard", JSON.stringify(arr));
// };

// saveScore(scoreBoardArray);
let getLocalStorageScoreboard;
if (localStorage.getItem("scoreboard" !== null)) {

    getLocalStorageScoreboard = JSON.parse(
        localStorage.getItem("scoreboard")
    );
    getLocalStorageScoreboard.sort(dynamicSort);
} else {
    getLocalStorageScoreboard = [];
}

const displayScoreElement = document.querySelector(".displayScore");
const nbPlayerInScoreboard = 10;
export const displayScore = () => {
    getLocalStorageScoreboard.forEach((player, i) => {
        if (i < nbPlayerInScoreboard) {
            tableBody.innerHTML += `
            <tr>
                <td>${i + 1}</td>
                <td>${player.name}</td>
                <td>${player.time}</td>
            </tr>
        `;
        }
    });
    
    if(displayScoreElement !== null) {
        displayScoreElement.style.display = "block";
    }
    
};

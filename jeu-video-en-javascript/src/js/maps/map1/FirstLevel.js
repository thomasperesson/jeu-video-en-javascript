import { Composite, Bodies, engine, Body, Events } from "../../MatterModule.js";
import { drawSaw } from "../../elements/Saw.js";
import { config } from "../../modules/config.js";
import { initBoulder } from "../../modules/Boulder.js";
import { initMusic } from "../../modules/LevelMusique.js";

export function initFirstLevel() {
    initMusic("1");
    var offset = 5;
    const backgroundImage = new Image();
    backgroundImage.src = "./src/img/maps/map1/backgrounds/lava2.jpg";
    const dragonImage = new Image();
    dragonImage.src = "./src/img/ennemis/dragon.png";
    const dragon2Image = new Image();
    dragon2Image.src = "./src/img/ennemis/dragon2.png";
    const golemImage = new Image();
    golemImage.src = "./src/img/ennemis/golem.png";
    const lavaImage = new Image();
    lavaImage.src = "./src/img/ennemis/lava.webp";
    const lava2Image = new Image();
    lava2Image.src = "./src/img/ennemis/lava2.webp";
    const leftDoorImage = new Image();
    leftDoorImage.src = `${config.imagePath.map2}/leftdoor.png`;
    const floortestImage = new Image();
    floortestImage.src = `${config.imagePath.map1}/platfloor.png`;
    const floorImage = new Image();
    floorImage.src = `${config.imagePath.map1}/floor2.png`;
    const wallImage = new Image();
    wallImage.src = `${config.imagePath.map1}/walltest.png`;
    const rampeImage = new Image();
    rampeImage.src = `${config.imagePath.map1}/rampe.png`;
    
    Composite.add(engine.world, [
        //background
        Bodies.rectangle(850, 0, 40, 40, {
            isStatic: true,
            render: {
                sprite: {
                    texture: backgroundImage.src,
                    xScale: 1.1,
                    yScale: 1,
                    yOffset: -0.5,
                },
            },
        }),
        //plafond
        Bodies.rectangle(400, -offset, 2950 + 2 * offset, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 1,
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 1.6,
                    yScale: 0.5,
                },
            },
        }),
        //sol
        Bodies.rectangle(400, 900 + offset, 2950 + 2 * offset, 50, {
            isStatic: true,
            label: "wall",
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 1.6,
                    yScale: 0.5,
                },
            },
        }),
        // mur droit
        Bodies.rectangle(1880 + offset, 300, 1600, 30 + 2 * offset, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 1.5,
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 0.7,
                    yScale: 0.35,
                },
            },
        }),
        // mur gauche
        Bodies.rectangle(+offset, 300, 1600, 50 + 2 * offset, {
            isStatic: true,
            angle: Math.PI * 0.5,
            label: "wall",
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 0.7,
                    yScale: 0.51,
                },
            },
        }),
    ]);

    Composite.add(engine.world, [
        Bodies.rectangle(160, 750, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(300, 187.5, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(160, 375, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(300, 562.5, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(450, 521, 718, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.5,
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.38,
                    yScale: 0.45,
                    yOffset: 0.05
                },
            },
        }),
        Bodies.rectangle(588, 855, 225, 50, {
            isStatic: true,
            label: "trap",
            angle: Math.PI * 0,
            render: {
                sprite: {
                    texture: lavaImage.src,
                    xScale: 0.27,
                    yScale: 0.20,
                    yOffset: -0.1
                },
            },
        }),
        Bodies.rectangle(726, 805, 150, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.5,
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.38,
                    yScale: 0.45,
                    xOffset: -0.40,
                    yOffset: 0.05
                },
            },
        }),
        Bodies.rectangle(726, 300, 600, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.5,
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.38,
                    yScale: 0.45,
                    xOffset: 0.085,
                    yOffset: 0.05
                },
            },
        }),
        Bodies.rectangle(900, 755, 300, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.6,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(950, 720, 150, 20, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.87,
            render: {
                sprite: {
                    texture: rampeImage.src,
                    xScale: 0.6,
                    yScale: 0.5,
                    xOffset: 0,
                    yOffset: -0.25,
                },
            },
        }),
        Bodies.rectangle(1225, 480, 600, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.5,
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.32,
                    yScale: 0.45,
                    xOffset: 0.002,
                    yOffset: 0.05
                },
            },
        }),
        Bodies.rectangle(1100, 280, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 1.87,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(875, 150, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(875, 480, 300, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.17,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.6,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1325, 200, 250, 50, {
            isStatic: true,
            label: "replaceboulder",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1575, 200, 250, 50, {
            isStatic: true,
            label: "replaceboulder",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1750, 500, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1500, 500, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1750, 810, 250, 50, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 2,
            render: {
                sprite: {
                    texture: floortestImage.src,
                    xScale: 0.5,
                    yScale: 0.4,
                },
            },
        }),
        Bodies.rectangle(1310, 855, 1110, 50, {
            isStatic: true,
            label: "trap",
            angle: Math.PI * 0,
            render: {
                sprite: {
                    texture: lava2Image.src,
                    xScale: 0.27,
                    yScale: 0.2,
                    yOffset: -0.1
                },
            },
        }),
        Bodies.rectangle(1700, 900, 40, 40, {
            isStatic: true,
            render: {
                sprite: {
                    texture: leftDoorImage.src,
                    xScale: 1,
                    yScale: 1,
                    xOffset: -0.12,
                    yOffset: 1.14,
                },
            },
        }),
    ]);

    var golem = Bodies.trapezoid(900, 100, 40, 40, 2, {
        isStatic: true,
        label: "trap2",
        angle: Math.PI * 2,
        render: {
            sprite: {
                texture: golemImage.src,
                xScale: 0.2,
                yScale: 0.2,
                xOffset: 0.3,
                yOffset: 0.08,
            },
        },
    });

    Events.on(engine, "beforeUpdate", function() {
        var py1 = 935 + 40 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(golem, { x: py1 - golem.position.x, y: 100 });
        Body.setAngularVelocity(golem, 0.02);
        Body.setPosition(golem, { x: py1, y: 100 });
        Body.rotate(golem, 0.00);
    });

    var dragon1 = Bodies.polygon(620, 195, 5, 60, {
        isStatic: true,
        label: "trap",
        angle: Math.PI * 0,
        render: {
            sprite: {
                texture: dragonImage.src,
                xScale: 0.15,
                yScale: 0.15,
            },
        },
    });

    Events.on(engine, "beforeUpdate", function() {
        var py1 = 600 + 120 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(dragon1, { x: 0, y: py1 - dragon1.position.y });
        Body.setAngularVelocity(dragon1, 0.02);
        Body.setPosition(dragon1, { x: 620, y: py1 });
        Body.rotate(dragon1, 0.00);
    });

    var dragon2 = Bodies.polygon(620, 195, 5, 60, {
        isStatic: true,
        label: "trap",
        angle: Math.PI * 0,
        render: {
            sprite: {
                texture: dragon2Image.src,
                xScale: 0.15,
                yScale: 0.15,
            },
        },
    });

    Events.on(engine, "beforeUpdate", function() {
        var py1 = 350 + 50 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(dragon2, { x: 0, y: py1 - dragon2.position.y });
        Body.setAngularVelocity(dragon2, 0.02);
        Body.setPosition(dragon2, { x: 1600, y: py1 });
        Body.rotate(dragon2, 0.00);
    });

    initBoulder();
    Composite.add(engine.world, [dragon1, dragon2, golem])
}
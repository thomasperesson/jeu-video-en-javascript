import { Composite, Bodies, engine, Body, Events } from "../../MatterModule.js";
import { drawSaw } from "../../elements/Saw.js";
import { config } from "../../modules/config.js";
import { Wall } from "../../elements/Wall.js";
import { initMusic } from "../../modules/LevelMusique.js";

export function initSecondLevel() {
    initMusic("2");
    var offset = 5;
    const backgroundImage = new Image();
    backgroundImage.src = "./src/img/maps/map2/backgrounds/background01.png";
    const wallImage = new Image();
    wallImage.src = `${config.imagePath.map2}/wall.png`;
    const slopeImage = new Image();
    slopeImage.src = `${config.imagePath.map2}/slope.png`;
    const blockImage = new Image();
    blockImage.src = `${config.imagePath.map2}/block.png`;
    const floorImage = new Image();
    floorImage.src = `${config.imagePath.map2}/floor.png`;
    const murImage = new Image();
    murImage.src = `${config.imagePath.map2}/murGD.png`;
    const sawImage = new Image();
    sawImage.src = `${config.imagePath.map2}/saw.png`;
    const spikePeHImage = new Image();
    spikePeHImage.src = `${config.imagePath.map2}/spike-pointe-en-haut.png`;
    const spikePeBImage = new Image();
    spikePeBImage.src = `${config.imagePath.map2}/spike-pointe-en-bas.png`;
    const spikePaGImage = new Image();
    spikePaGImage.src = `${config.imagePath.map2}/spike-pointe-a-gauche.png`;
    const spikePaDImage = new Image();
    spikePaDImage.src = `${config.imagePath.map2}/spike-pointe-a-droite.png`;
    const platformImage = new Image();
    platformImage.src = `${config.imagePath.map2}/platform.png`;
    const leftDoorImage = new Image();
    leftDoorImage.src = `${config.imagePath.map2}/leftdoor.png`;

    const ceil = new Wall(
        400, -offset,
        2950 + 2 * offset,
        50,
        Math.PI,
        floorImage.src,
        2.8,
        1.25,
        "wall"
    );

    const floor = new Wall(
        400,
        900 + offset,
        2950 + 2 * offset,
        50,
        0,
        floorImage.src,
        2.8,
        1.25,
        "wall"
    );

    const rightWall = new Wall(
        1880 + offset,
        300,
        30,
        1600 + 2 * offset,
        Math.PI,
        murImage.src,
        0.9,
        1.2,
        "wall"
    );

    const leftWall = new Wall(
        offset,
        300,
        50,
        1600 + 2 * offset,
        0,
        murImage.src,
        1.3,
        1.2,
        "wall"
    );

    Composite.add(engine.world, [
        Bodies.rectangle(850, 0, 40, 40, {
            isStatic: true,
            render: {
                sprite: {
                    texture: backgroundImage.src,
                    xScale: 1.1,
                    yScale: 1,
                    yOffset: -0.5,
                },
            },
        }),
        //plafond
        ceil.draw(),

        //sol
        floor.draw(),

        // mur droit
        rightWall.draw(),

        // mur gauche
        leftWall.draw(),
    ]);

    // création de tableau pour évité d'avoir 50000 lignes
    const floorSpikes = []; // création de la const floorSpikes qui est un tableau '[]'
    for (let i = 0; i <= 7; i++) {
        // for(pour chaque iteration de la boucle)
        floorSpikes.push(
            Bodies.polygon(230 + i * 35, 870, 3, 20, {
                isStatic: true,
                angle: Math.PI * 0.5,
                label: "trap",
                render: {
                    sprite: {
                        texture: spikePeHImage.src,
                        xScale: 0.6,
                        yScale: 1,
                        xOffset: 0.19,
                        yOffset: -0.2,
                    },
                },
            })
        );
    }

    const movingplatformSpikes = []; // création de la const floorSpikes qui est un tableau '[]'
    for (let i = 0; i <= 33; i++) {
        // for(pour chaque iteration de la boucle)
        movingplatformSpikes.push(
            Bodies.polygon(695 + i * 35, 470, 3, 20, {
                isStatic: true,
                angle: Math.PI * 0.5,
                label: "trap",
                render: {
                    sprite: {
                        texture: spikePeHImage.src,
                        xScale: 0.6,
                        yScale: 1,
                        xOffset: 0.19,
                        yOffset: -0.2,
                    },
                },
            })
        );
    }
    const roofSpikes = [];
    for (let i = 0; i <= 7; i++) {
        roofSpikes.push(
            Bodies.polygon(50 + i * 35, 30, 3, 20, {
                isStatic: true,
                angle: Math.PI * 0.5,
                label: "trap",
                render: {
                    sprite: {
                        texture: spikePeBImage.src,
                        xScale: 0.6,
                        yScale: 1,
                        xOffset: -0.19,
                        yOffset: -0.2,
                    },
                },
            })
        );
    }
    const firstSegmentSpikes = [];
    for (let i = 1; i <= 6; i++) {
        if (i % 2 != 0) {
            firstSegmentSpikes.push(
                Bodies.polygon(179, 200 + i * 100, 3, 20, {
                    isStatic: true,
                    angle: Math.PI * 0,
                    label: "trap",
                    render: {
                        sprite: {
                            texture: spikePeHImage.src,
                            xScale: 0.6,
                            yScale: 1,
                            xOffset: 0.05,
                            yOffset: -0.36,
                        },
                    },
                })
            );
        } else {
            firstSegmentSpikes.push(
                Bodies.polygon(41, 200 + i * 100, 3, 20, {
                    isStatic: true,
                    angle: Math.PI * 1,
                    label: "trap",
                    render: {
                        sprite: {
                            texture: spikePeHImage.src,
                            xScale: 0.6,
                            yScale: 1,
                            xOffset: 0.35,
                            yOffset: 0.02,
                        },
                    },
                })
            );
        }
    }
    const seg2TopSpikes = [];
    for (let i = 0; i <= 2; i++) {
        seg2TopSpikes.push(
            Bodies.polygon(485, 300 - i * 40, 3, 20, {
                isStatic: true,
                angle: Math.PI * 0,
                label: "trap",
                render: {
                    sprite: {
                        texture: spikePeHImage.src,
                        xScale: 0.6,
                        yScale: 1,
                        xOffset: 0,
                        yOffset: 0,
                    },
                },
            })
        );
    }

    const jumpOverSpikes = [];
    for (let i = 0; i <= 5; i++) {
        jumpOverSpikes.push(
            Bodies.polygon(900 + i * 35, 170, 3, 20, {
                isStatic: true,
                angle: Math.PI * 0.5,
                label: "trap",
                render: {
                    sprite: {
                        texture: spikePeHImage.src,
                        xScale: 0.6,
                        yScale: 1,
                        xOffset: 0.19,
                        yOffset: -0.2,
                    },
                },
            })
        );
    }
    const drawStaticSaw = (x, y) => {
        const staticSaw = Bodies.circle(x, y, 50, {
            isStatic: true,
            angle: Math.PI * 0.5,
            label: "trap",
            render: {
                sprite: {
                    texture: sawImage.src,
                    xScale: 0.68,
                    yScale: 0.68,
                },
            },
        });
        return staticSaw;
    };

    // platforme qui fait des gauche, droite
    const movingplatform = Bodies.rectangle(1690, 290, 100, 30, {
        isStatic: true,
        label: "wall",
        render: {
            sprite: {
                texture: platformImage.src,
                xScale: 1.2,
                yScale: 0.6,
            },
        },
    });

    // saws
    const saw1 = drawSaw();
    const saw2 = drawSaw();
    const saw3 = drawSaw();
    const saw4 = drawSaw();
    const saw5 = drawSaw();
    const saw6 = drawSaw();
    const staticSaw1 = drawStaticSaw(200, 230);
    const staticSaw2 = drawStaticSaw(325, 460);
    // Tourniquet
    const py = 10;
    const tourniquet = Bodies.rectangle(980, py, 200, 20, {
        isStatic: true,
        render: {
            sprite: {
                texture: wallImage.src,
                xScale: 0.27,
                yScale: 1,
            },
        },
    });

    //Mouvement des scies
    Events.on(engine, "beforeUpdate", function() {
        // platforme qui fait des gauche, droite
        const movingplatformdirection =
            1300 + 600 * Math.sin(engine.timing.timestamp * 0.0008);
        Body.setVelocity(movingplatform, {
            y: 0,
            x: movingplatformdirection - movingplatform.position.x,
        });
        Body.setPosition(movingplatform, {
            y: 390,
            x: movingplatformdirection,
        });

        //saw1
        let py1 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(saw1, { x: 0, y: py1 - saw1.position.y });
        Body.setAngularVelocity(saw1, 0.02);
        Body.setPosition(saw1, { x: 800, y: py1 });
        Body.rotate(saw1, 0.02);

        //saw2
        let py2 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.004);
        Body.setVelocity(saw2, { x: 0, y: py2 - saw2.position.y });
        Body.setAngularVelocity(saw2, 0.02);
        Body.setPosition(saw2, { x: 950, y: py2 });
        Body.rotate(saw2, 0.04);

        //saw3
        let py3 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.003);
        Body.setVelocity(saw3, { x: 0, y: py3 - saw3.position.y });
        Body.setAngularVelocity(saw3, 0.02);
        Body.setPosition(saw3, { x: 1100, y: py3 });
        Body.rotate(saw3, 0.03);

        //saw4
        let py4 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.002);
        Body.setVelocity(saw4, { x: 0, y: py4 - saw4.position.y });
        Body.setAngularVelocity(saw4, 0.02);
        Body.setPosition(saw4, { x: 1250, y: py4 });
        Body.rotate(saw4, 0.02);

        //saw5
        let py5 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.004);
        Body.setVelocity(saw5, { x: 0, y: py5 - saw5.position.y });
        Body.setAngularVelocity(saw5, 0.02);
        Body.setPosition(saw5, { x: 1400, y: py5 });
        Body.rotate(saw5, 0.04);

        //saw6
        let py6 = 700 + 120 * Math.sin(engine.timing.timestamp * 0.003);
        Body.setVelocity(saw6, { x: 0, y: py6 - saw6.position.y });
        Body.setAngularVelocity(saw6, 0.02);
        Body.setPosition(saw6, { x: 1550, y: py6 });
        Body.rotate(saw6, 0.03);

        //staticSaws
        Body.rotate(staticSaw1, 0.05);
        Body.rotate(staticSaw2, 0.05);

        //tourniquet
        Body.setVelocity(tourniquet, {
            x: 0,
            y: py - tourniquet.position.y,
        });
        Body.setAngularVelocity(tourniquet, 0.02);
        Body.setPosition(tourniquet, { x: 980, y: py });
        Body.rotate(tourniquet, 0.02);
    });

    Composite.add(engine.world, [
        Bodies.rectangle(200, 750, 1000, 20, {
            isStatic: true,
            label: "wall",
            angle: Math.PI * 0.5,
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 1.5,
                    yScale: 1.08,
                },
            },
        }),

        Bodies.rectangle(505, 750, 1100, 20, {
            isStatic: true,
            angle: -Math.PI * 0.5,
            label: "wall",
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 1.5,
                    yScale: 1.08,
                },
            },
        }),
        Bodies.rectangle(1045, 200, 1100, 40, {
            isStatic: true,
            angle: -Math.PI * 0,
            label: "wall",
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 1,
                    yScale: 1,
                },
            },
        }),
        Bodies.rectangle(800, 160, 120, 20, {
            isStatic: true,
            angle: -Math.PI * 0.15,
            label: "wall",
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.16,
                    yScale: 0.9,
                },
            },
        }),
        Bodies.rectangle(325, 150, 700, 20, {
            isStatic: true,
            angle: Math.PI * 0.5,
            label: "wall",
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.9,
                    yScale: 1.08,
                },
            },
        }),

        Bodies.polygon(221, 700, 3, 20, {
            isStatic: true,
            angle: Math.PI * 1,
            label: "trap",
            render: {
                sprite: {
                    texture: spikePeHImage.src,
                    xScale: 0.6,
                    yScale: 1,
                    xOffset: 0.35,
                    yOffset: 0.02,
                },
            },
        }),
        Bodies.polygon(485, 700, 3, 20, {
            isStatic: true,
            angle: Math.PI * 0,
            label: "trap",
            render: {
                sprite: {
                    texture: spikePeHImage.src,
                    xScale: 0.6,
                    yScale: 1,
                    xOffset: 0,
                    yOffset: 0,
                },
            },
        }),
        Bodies.rectangle(1900, 150, 600, 40, {
            isStatic: true,
            angle: Math.PI * 0.7,
            label: "wall",
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.8,
                    yScale: 2,
                },
            },
        }),
        Bodies.rectangle(1400, 500, 1450, 40, {
            isStatic: true,
            angle: Math.PI * 0,
            label: "wall",
            render: {
                sprite: {
                    texture: floorImage.src,
                    xScale: 1.318,
                    yScale: 1,
                },
            },
        }),
        Bodies.rectangle(695, 645, 250, 40, {
            isStatic: true,
            angle: Math.PI * 0.5,
            label: "wall",
            render: {
                sprite: {
                    texture: wallImage.src,
                    xScale: 0.333,
                    yScale: 2,
                },
            },
        }),
        Bodies.rectangle(1700, 900, 40, 40, {
            isStatic: true,
            render: {
                sprite: {
                    texture: leftDoorImage.src,
                    xScale: 1,
                    yScale: 1,
                    xOffset: -0.12,
                    yOffset: 1.14,
                },
            },
        }),
        Bodies.rectangle(1765, 835, 200, 90, {
            isStatic: true,
            angle: Math.PI * 2,
            label: "wall",
            render: {
                sprite: {
                    texture: blockImage.src,
                    xScale: 0.75,
                    yScale: 0.75,
                    xOffset: 0,
                    yOffset: 0,
                },
            },
        }),
        Bodies.trapezoid(1635, 850, 90, 90, 2, {
            isStatic: true,
            angle: Math.PI * 1.5,
            label: "wall",
            render: {
                sprite: {
                    texture: slopeImage.src,
                    xScale: 0.58,
                    yScale: 0.6,
                    xOffset: -0.33,
                    yOffset: 0.01,
                },
            },
        }),
        staticSaw1,
        staticSaw2,
        saw1,
        saw2,
        saw3,
        saw4,
        saw5,
        saw6,
        movingplatform,
        tourniquet,
    ]);

    Composite.add(engine.world, floorSpikes);
    Composite.add(engine.world, roofSpikes);
    Composite.add(engine.world, movingplatformSpikes);
    Composite.add(engine.world, firstSegmentSpikes);
    Composite.add(engine.world, seg2TopSpikes);
    Composite.add(engine.world, jumpOverSpikes);
}
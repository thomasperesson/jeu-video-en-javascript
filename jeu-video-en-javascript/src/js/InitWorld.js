import { initTimer } from "./modules/Timer.js";
import { drawWorld } from "./modules/World.js";
import { initFirstLevel } from "./maps/map1/FirstLevel.js";
import { initSecondLevel } from "./maps/map2/SecondLevel.js";
import { initPlayer } from "./modules/Player.js";
import { drawFinish } from "./elements/Finish.js";

export function initWorld(actualLevel = "1") {
    //Create world
    drawWorld();
    switch (actualLevel) {
        case "1":
            //Create first level
            initFirstLevel();
            initPlayer();
            drawFinish();
            break;
        case "2":
            initSecondLevel();
            initPlayer();
            drawFinish(1817, 705);
            break;
        default:
            //Create first level
            initFirstLevel();
            initPlayer();
            break;
    }

    //initie le timer du jeu
    initTimer();
    drawFinish();
    // const deathCounter = document.getElementById("deathCounter");
    // deathCounter.innerText = localStorage.getItem("deathCounter");
}
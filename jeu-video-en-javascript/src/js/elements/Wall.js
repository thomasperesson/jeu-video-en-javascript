import { Bodies } from "../MatterModule.js";

export class Wall {
    constructor(
        xPos,
        yPos,
        width,
        height,
        angle,
        img,
        xScale,
        yScale,
        label
    ) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.angle = angle;
        this.img = img;
        this.xScale = xScale;
        this.yScale = yScale;
        this.label = label;
    }

    draw() {
        return Bodies.rectangle(this.xPos, this.yPos, this.width, this.height, {
            isStatic: true,
            label: this.label,
            angle: this.angle,
            render: {
                sprite: {
                    texture: this.img,
                    xScale: this.xScale,
                    yScale: this.yScale,
                },
            },
        });
    }
}

import { Bodies, Body } from "../MatterModule.js";
import { config } from "../modules/config.js";

const sawImage = new Image();
sawImage.src = `${config.imagePath.map2}/saw.png`;

export function drawSaw() {
    const partA = Bodies.polygon(500, 187, 3, 50, {
        angle: Math.PI * 0.5,
        label: "trap",
        render: {
            fillStyle: "grey"
        }
    });
    const partB = Bodies.polygon(500, 187, 3, 50, {
        angle: Math.PI * 1,
        label: "trap",
        render: {
            fillStyle: "grey"
        }
    });
    const partC = Bodies.polygon(500, 187, 3, 50, {
        angle: Math.PI * 1.5,
        label: "trap",
        render: {
            fillStyle: "grey"
        }
    });
    const partD = Bodies.polygon(500, 187, 3, 50, {
        angle: Math.PI * 2,
        label: "trap",
        render: {
            fillStyle: "grey"
        }
    });
    const partE = Bodies.polygon(500, 187, 3, 50, {
        angle: Math.PI * 2.5,
        label: "trap",
        render: {
            sprite: {
                texture: sawImage.src,
                xScale: 0.68,
                yScale: 0.68,
                yOffset: -0.15,
                xOffset: -0.01,
            },
        },
    });
    const saw = Body.create({
        parts: [partA, partB, partC, partD, partE],
        isStatic: true,

    });
    return saw;
}
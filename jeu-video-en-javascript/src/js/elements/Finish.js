import { Composite, Bodies, engine } from "../MatterModule.js";
import { config } from "../modules/config.js";

export const drawFinish = (xPos = 1817, yPos = 705) => {
    const rightDoorImage = new Image();
    rightDoorImage.src = `${config.imagePath.map2}/rightdoor.png`;
    let finishLabel;
    switch (localStorage.getItem("actualLevel")) {
        case "1":
            finishLabel = "nextlevel";
            break;
        case "2":
            finishLabel = "finish";
            break;
        default:
            finishLabel = "finish";

    }
    Composite.add(engine.world, [
        Bodies.rectangle(xPos, yPos, 10, 100, {
            isStatic: true,
            label: finishLabel,
            render: {
                sprite: {
                    texture: rightDoorImage.src,
                    xScale: 1,
                    yScale: 1,
                    // xOffset: 0.24,
                },
            },
        }),
    ]);
};